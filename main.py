#!/usr/bin/python3

import logging
import time
from datetime import datetime
import os

import google
import bot
from bot import bot as mybot
from telepot.loop import MessageLoop


# --------------------------------------------------------------------------------
log_file = '/home/qleon/GIT/sim_balancer/log.txt'  # to do


log = logging.getLogger("Main_Sim_Balancer")
log.setLevel(logging.DEBUG)

fh = logging.FileHandler(log_file)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)

log.addHandler(fh)
log.warning("Program started")

hot_calc_bot = None
fix_need = None

# --------------------------------------------------------------------------------

# Функция высчитывает от текущяего дня недели +2 дня для подсчёта заполненных номеров
# Если это выпадает на выходной день - переносит на следующую неделю
# Номер дня недели [0(Воскресеньн),6(Суббота)]
def calc_payday(cur_week_day):
    pay_day = cur_week_day + 2
    if pay_day >= 6:
        return cur_week_day - 3
    else:
        return pay_day

def handle(msg):
    global hot_calc_bot, fix_need

    from_first_name = msg['from']['first_name']
    from_id = msg['from']['id']
    command = msg['text'].strip().lower()
    log.info("bot. From:{}  Message:{}".format(from_first_name, command))

    if from_id == 125651712:
        if command == 'calc':
            hot_calc_bot = True
            log.info("bot. Command confirmed {}".format(command))

        if command == 'send_to_all':
            fix_need = True
            log.info("bot. Command confirmed {}".format(command))

# --------------------------------------------------------------------------------

def main_loop():
    global hot_calc_bot, fix_need

    while True:
        log.info('*****************Loop***********************')

        last_run = os.environ.get('SIM_BOT_LAST_RUN')
        pay_day = os.environ.get('SIM_BOT_PAY_DAY')
        need_run = os.environ.get('SIM_BOT_NEED_RUN')


        date, month, year, weekday, hour, min, sec = list(
            map(int, datetime.today().strftime("%d %m %Y %w %H %M %S").split()))
        today = "{}_{}_{}".format(date, month, year)

        log.info("{}_{}_{} weekday:{} {}:{}:{}".format(date, month, year, weekday, hour, min, sec))

        log.info('today: {}'.format(today))
        log.info('last_run: {}'.format(last_run))
        log.info('pay_day: {}'.format(pay_day))
        log.info('need_run: {}'.format(need_run))

        # --WARNING Вставить сюда переменные если сервер не был запущен и нужно сегодня запустить ---

        # --WARNING-Проверка если нужно запустить только сегодня-------------------------------------
        if fix_need:
            log.warning("bot WARNING fix_need is True")
            if last_run != today:
                log.info('bot WARNING fix_need because - message: send_to_all ')
                os.environ['SIM_BOT_NEED_RUN'] = "True"

        # -----ОБЫЧНАЯ РАБОТА СКРИПТА------------------------------------------------
        else:  # Основной запуск если это 25 число, и запуск не был сегодня
            if date == 25 and last_run != today:
                log.info('Today {} day to send all'.format(date))

                os.environ['SIM_BOT_NEED_RUN'] = "True"
            else:
                os.environ['SIM_BOT_NEED_RUN'] = "False"
                log.info('Wait 25 day to send all')


        # Если пора запускать - ждём 10 утра и будний день
        if need_run == "True":
            log.info('---Wait workday and 10:30 --- or if fix_need: {}'.format(fix_need))

            # Номер дня недели [0(Воскресеньн),6(Суббота)]
            if (hour == 10 and min == 30 and weekday not in [0, 6]) or fix_need:
                log.info('---------------Add new column in google table-------')
                google.add_new_date("Остаток на {}".format(today))
                time.sleep(5)
                log.info('---------------Send message in chats----------------')
                bot.send_to_chats()

                # Флаг чтоб ещё раз не запустить сегодня
                os.environ['SIM_BOT_LAST_RUN'] = "{}_{}_{}".format(date, month, year)
                os.environ['SIM_BOT_PAY_DAY'] = "".format(calc_payday(weekday))

                fix_need = False

        log.debug("Wait weekday Pay_day: {}. (0-Вск 6-Сбт)".format(pay_day))

        # Ждём 2 дня после заполнения таблицы и считаем сумму
        if (pay_day == str(weekday) and hour == 11 and min == 30) or hot_calc_bot:
            log.info('-----Calculate sum------')
            numbers, summ = google.find_tel_nums(google.auth())
            log.info('-----Send sum to owner in telegram ------')
            bot.send_owner_sum(numbers, summ)
            os.environ['SIM_BOT_PAY_DAY'] = "None"
            hot_calc_bot = False
            time.sleep(60)

        time.sleep(10)  # main loop sleep


if __name__ == '__main__':
    try:
        MessageLoop(mybot, handle).run_as_thread()
        main_loop()

    except Exception as e:
        log.critical("Error : {}".format(e))
