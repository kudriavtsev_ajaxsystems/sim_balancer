import telepot
import time
import logging


main_logger = logging.getLogger('Main_Sim_Balancer')
log = logging.getLogger('Main_Sim_Balancer.bot')
log.setLevel(logging.DEBUG)


TOKEN = '451732595:AAHULBRwkoTFwpnAAsik6wwZDWm1wWWq1_g'

# 'QA_Team', 'id': -1001089581961
# QA_DEVICE_TEAM, 'id': -1001343656436
# Братство хабца, 'id': -191945197

chat_id_lst = ['125651712', '-1001089581961', '-1001343656436', '-191945197']
owner_id = '125651712'  # Кудрявцев


bot = telepot.Bot(TOKEN)


def send_to_chats():
    log.info("Список чатов {}".format(chat_id_lst))
    try:
        for chat_id in chat_id_lst:
            bot.sendMessage(chat_id, '💵 Колеги, прошу сьогодні заповнити таблицю поповнення сімкарт!'
                                     '\nhttps://docs.google.com/spreadsheets/d/1BZeXKfmEPUd6lCVZb_MZTeHMkdw67QZxIYIDQKkOE9Y/edit#gid=1115747446&range=A1')
        log.info('Bot successfully sent message with link to chats ')
    except Exception as e:
        log.warning("Bot send_to_chats ERROR: {}".format(e))
    finally:
        time.sleep(3)


def send_owner_sum(data_nums, data_summ):
    try:
        bot.sendMessage(owner_id, data_nums)
        time.sleep(1)
        bot.sendMessage(owner_id, data_summ)
        log.info('Bot successfully send_owner_sum numbers and summ to {}'.format(owner_id))
    except Exception as e:
        log.warning("Bot send_owner_sum ERROR: {}".format(e))
    finally:
        time.sleep(3)
