import gspread
from oauth2client.service_account import ServiceAccountCredentials
import math
import logging
import sys

main_logger = logging.getLogger('Main_Sim_Balancer')
log = logging.getLogger('Main_Sim_Balancer.google')
log.setLevel(logging.DEBUG)


auth_file = 'auth.json'
g_sheet_key = '1BZeXKfmEPUd6lCVZb_MZTeHMkdw67QZxIYIDQKkOE9Y'
g_wsheet_name = "Sim_balance_2020"


def auth():
    # use creds to create a client to interact with the Google Drive API
    scope = ['https://spreadsheets.google.com/feeds',
             'https://www.googleapis.com/auth/drive']
    try:
        credo = ServiceAccountCredentials.from_json_keyfile_name(auth_file, scope)

        client = gspread.authorize(credo)
        sheet_object = client.open_by_key(g_sheet_key)
        sheet = sheet_object.worksheet(g_wsheet_name)
        log.debug('Google auth successfully')

    except Exception as e:
        log.critical("EXIT. Error:{} ".format(e))
        sys.exit(1)

    return sheet



def add_new_date(today):
    sheet = auth()
    sheet.update_cell(1, len(sheet.row_values(1)) + 1, today)
    log.info("Google add new date '{}' successfully".format(today))


#---------------------------------------------------------------------

def find_tel_nums(sheet):
    list_of_lists = sheet.get_all_values()
    header_len = len(list_of_lists[0])

    numbers_list = []
    for list_ in list_of_lists[1:]:
        if not list_[header_len - 1]:
            continue
        a = list_[header_len - 1] = list_[header_len - 1].replace(',', '.')
        try:
            float(a)
        except Exception:
            continue
        inner = []
        for el in list_:
            inner.append(el)
        numbers_list.append(inner)

    return my_math(numbers_list)


def my_math(list):
    d = {}
    for el in list:
        money = math.ceil(30 - float(el[-1]))
        if money > 0:
            a, b = divmod(money, 5)
            if b == 0:
                pass
            else:
                money = 5 * (a + 1)

            d.setdefault(el[0], money * 2)

    from collections import OrderedDict
    nums_sorted_by_value = OrderedDict(sorted(d.items(), key=lambda x: x[1]))
    summ = 0

    data_nums = "Номера для пополнения: \n"

    for k, v in nums_sorted_by_value.items():

        data_nums += "{} - {}\n".format(k, v)
        summ += int(v)

    data_summ = 'Сумма = {}'.format(summ)
    log.info(data_nums)
    log.info(data_summ)

    return data_nums, data_summ
